#pragma once
#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>

namespace std::chrono {
    typedef duration<int, ratio_multiply<hours::period, ratio<24> >::type> days;
}

namespace stats_bot
{

    struct Items {
        int truth() {
            return 42;
        }
    };

    struct DayUserStats: Items {
        long id;
        std::chrono::days date;
        long msg_count;
        /* User user; */

        friend std::ostream& operator<<(std::ostream& os, DayUserStats const & m)
        {
            return os <<
                /* "\n\t- Date: " << m.date << */
                "\n\t- Messages: " << m.msg_count;
        }
    };

    struct DayChatStats {
        long id;
        std::chrono::days date;
        long msg_count;
        /* Chat chat; */

        DayChatStats(long id, std::chrono::days date, long msg_count): id(id), date(date), msg_count(msg_count){}

        friend std::ostream& operator<<(std::ostream& os, DayChatStats const & m)
        {
            return os <<
                /* "\n\t- Date: " << m.date << */
                "\n\t- Messages: " << m.msg_count;
        }
    };

    struct UserChatStats {
        long id;
        long total_msg_cnt;

        friend std::ostream& operator<<(std::ostream& os, UserChatStats const & u)
        {
            os << "Chat Stats [" << u.id << "] - Total Messages: " << u.total_msg_cnt;
            return os;
        }
    };

    struct Membership {
        long id;
        bool removed;
        /* Chat chat; */
        /* User user; */
        UserChatStats user_chat_stats;

        friend std::ostream& operator<<(std::ostream& os, Membership const & m)
        {
            return os
                << " \t- Messages: "
                << m.user_chat_stats.total_msg_cnt;
        }
    };

    struct User {
        long id;
        std::string name;
        bool is_bot;
        std::vector<std::shared_ptr<DayUserStats>> dayUserStats;
        std::vector<std::shared_ptr<Membership>> memberships;

        friend std::ostream& operator<<(std::ostream& os, User const & u)
        {
            os << u.name << " [" << u.id << "]";
            return os;
        }
    };

    struct Chat {
        long id;
        std::string title;
        std::string username;
        //id is timestamp/24
        std::map<std::chrono::days, std::shared_ptr<DayChatStats>> dayChatStats;
        std::map<std::chrono::days, std::shared_ptr<Membership>> memberships;

        Chat(long id, std::string title, std::string username): id(id), title(title), username(username), dayChatStats({}), memberships({})
        {}

        friend std::ostream& operator<<(std::ostream& os, Chat const & c)
        {
            os << c.title << " [" << c.id << "] - " << c.username << ":" << std::endl;
            std::cout << "a";
            /* for ( auto it= c.dayChatStats.begin(); it != c.dayChatStats.end(); ++it ) { */
            for(auto dayChatStat: c.dayChatStats){
                os << *(dayChatStat.second);
                /* os << *dayChatStat; */
                std::cout << "b";
            }
            return os;
        }
    };

}
