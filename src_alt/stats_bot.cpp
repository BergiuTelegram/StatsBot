#include <iostream>
#include <string>
#include <unordered_map>
#include <tgbot/tgbot.h>
#include "db_types.hpp"
#include <chrono>
#include <map>

using namespace std;

namespace stats_bot
{

    struct StatsBot {
        map<long, shared_ptr<User>> users;
        map<long, shared_ptr<Chat>> chats;

        void addMessageToChats(TgBot::Message::Ptr& message){
            auto tg_chat = message->chat;
            long chat_id = tg_chat->id;
            auto chat_tuple = this->chats.find(chat_id);
            if( chat_tuple==this->chats.end() ){
                //new chat
                //create new chat (make_shared)
                string title = message->chat->title;
                string username = message->chat->username;
                shared_ptr<Chat> new_chat = make_shared<Chat>(chat_id, title, username);
                //add new chat
                auto pair = this->chats.emplace(chat_id, new_chat);
                if( pair.second ){
                    cout << "added new chat" << endl;
                }
                //get new chat_tuple
                //chat_tuple = neues chat_tuple
                chat_tuple = this->chats.find(chat_id);
            }
            //chat_tuple should now be set, but test it
            if( chat_tuple==this->chats.end() ){
                //something went wrong
                cout << "ERROR" << endl;
                cout << "\t- kein chat" << endl;
                return;
            }
            shared_ptr<Chat> chat = chat_tuple->second;
            //get day
            chrono::days days_till_now = chrono::duration_cast< chrono::days >(
                chrono::system_clock::now().time_since_epoch()
            );
            cout << days_till_now.count() << endl;
            chrono::days date = chrono::days{ message->date/60/60/24 };
            cout << date.count() <<endl;
            //search for related dayChatStats
            //auto dayChatStats = ...
            auto dayChatStats_tuple = chat->dayChatStats.find(date);
            //if day chat stats not exists
            if( dayChatStats_tuple==chat->dayChatStats.end() ){
                //create one
                shared_ptr<DayChatStats> new_dayChatStat = make_shared<DayChatStats>(0, days_till_now, 1);
                //add to chat
                auto pair = chat->dayChatStats.emplace(days_till_now, new_dayChatStat);
                if( pair.second ){
                    cout << "added new chat" << endl;
                }
                //add to outer scope
                //get new chat_tuple
                //chat_tuple = neues chat_tuple
                dayChatStats_tuple = chat->dayChatStats.find(date);
                cout << "kein day chat stats" << endl;
                cout << *new_dayChatStat << endl;
            }
            //if day chat stats not exists
            if( dayChatStats_tuple==chat->dayChatStats.end() ){
                //something went wrong
                cout << "ERROR" << endl;
                cout << "\t- kein day chat stats" << endl;
                return;
            }
            shared_ptr<DayChatStats> dayChatStat = dayChatStats_tuple->second;
            //increase dayChatStats message counter
            dayChatStat->msg_count+=1;
        }

        void addMessageToUser(TgBot::Message::Ptr& message){
                /* User user{user_id, t_user->firstName, 0}; */
        }

    };

}
