#include <iostream>
#include <vector>
#include <signal.h>
#include <stdio.h>
#include <exception>
#include <tgbot/tgbot.h>
#include <unordered_map>
#include <streambuf>

using namespace std;

namespace file_connector
{
    struct FileConnector {

        static vector<vector<string>> readFromFile(string filename){
            // READ TOKEN FROM FILE
            ifstream inFile(filename);
            vector<vector<string>> korb;
            if(!inFile.is_open()){
                cerr << "error opening file (" << filename << ")" << endl;
                //TODO throw error
                return korb;
            }
            stringstream ss;
            //after all lines there is another round befor eof
            while(!inFile.eof()){
                //lies die ganze datei
                string line;
                //Zeile für Zeile
                getline(inFile, line);
                ss<<line<<endl;
                istringstream line_stream(line);
                string attr;
                vector<string> attributes;
                while(std::getline(line_stream, attr, ';')) {
                    attributes.push_back(attr);
                }
                korb.push_back(attributes);
            }
            cout << "stringstream:" << endl << ss.str() << endl << "korb:" << endl;
            return korb;
        }

    };
}
