#include <iostream>
#include <vector>
#include <signal.h>
#include <stdio.h>
#include <exception>
#include <tgbot/tgbot.h>
#include "db_types.hpp"
#include <unordered_map>
#include <streambuf>
#include "stats_bot.cpp"
#include "file_connector.cpp"

using namespace stats_bot;
using namespace file_connector;
/* using namespace std; */
/* using namespace TgBot; */




int main(int argc, char ** argv)
{
    // READ TOKEN FROM FILE
    std::ifstream t("TOKEN");
    if(!t.is_open()){
        std::cerr << "error opening file (TOKEN)" << std::endl;
        return 0;
    }
    std::string token;
    getline(t, token);
    std::cout << "[token:" << token << "]" << std::endl;

    /* std::shared_ptr<std::unordered_map<long, User>> users; */
    auto statsBot = std::make_shared<StatsBot>();

    TgBot::Bot bot(token);
    bot.getEvents().onCommand("start", [&bot](TgBot::Message::Ptr message) {
        bot.getApi().sendMessage(message->chat->id, "Hi!");
    });
    /* bot.getEvents().onCommand("stats", [&bot](TgBot::Message::Ptr message) { */
    bot.getEvents().onCommand("stats", [&](TgBot::Message::Ptr message) {
        std::cout << "/stats" << std::endl;
        std::stringstream out;
        long id = message->chat->id;
        auto chat_tuple = statsBot->chats.find(id);
        if( chat_tuple != statsBot->chats.end() ) {
            std::cout << "/stats chat gefunden" << std::endl;
            out << *(chat_tuple->second) << std::endl;
        } else {
            std::cout << "CHAT Not found: " << id << std::endl;
        }
        /* for(auto dayChatStat: chat_tuple->second.id){ */
            /* if( dayChatStat.second.chat.id == message->chat->id ){ */
            /*     std::cout << dayChatStat.second << std::endl; */
            /*     out << dayChatStat.second << std::endl; */
            /* } */
        /* } */
        bot.getApi().sendMessage(message->chat->id, out.str());
    });
    bot.getEvents().onAnyMessage([&](TgBot::Message::Ptr message) {
        std::cout << "-----------" << std::endl << "new message" << std::endl;
        // chat
        statsBot->addMessageToChats(message);
        // TODO
        // user
        if( message->from != nullptr ){
            statsBot->addMessageToUser(message);
            // membership, if chat und user exists
            // userChatStats, mit membership verbinden
        }
        /* if (StringTools::startsWith(message->text, "/start")) { */
        /*     return; */
        /* } */
        /* bot.getApi().sendMessage(message->chat->id, "Your message is: " + message->text); */
    });

    try {
        printf("Bot username: %s\n", bot.getApi().getMe()->username.c_str());
        TgBot::TgLongPoll longPoll(bot);
        while (true) {
            printf("Long poll started\n");
            longPoll.start();
        }
    } catch (TgBot::TgException& e) {
        printf("error: %s\n", e.what());
    }
    return 0;
}
