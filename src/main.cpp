#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "json.hpp"
#include "chrono.hpp"
#include "stats_bot/FileConnector.hpp"
#include "stats_bot/items/Item.hpp"
#include "stats_bot/items/Chat.hpp"
#include "stats_bot/items/DayChatStat.hpp"
#include "stats_bot/items/DayUserStat.hpp"
#include "stats_bot/items/Membership.hpp"
#include "stats_bot/items/User.hpp"
#include "stats_bot/items/UserChatStat.hpp"
#include "stats_bot/controller/Controller.hpp"
#include "stats_bot/controller/ChatCtrl.hpp"
#include "stats_bot/controller/DayUserStatCtrl.hpp"
#include "stats_bot/controller/UserChatStatCtrl.hpp"
#include "stats_bot/controller/DayChatStatCtrl.hpp"
#include "stats_bot/controller/MembershipCtrl.hpp"
#include "stats_bot/controller/UserCtrl.hpp"
#include "stats_bot/controller/ControllerCtrl.hpp"

using json = nlohmann::json;

using namespace stats_bot::items;
using namespace stats_bot::controller;
int main()
{
	std::cout << "----------------" << std::endl;
	std::cout << "Welcome my the Telegram Stats Bot." << std::endl;
	std::cout << "----------------" << std::endl;
	std::cout << "Starting Stats Bot" << std::endl;
	ControllerCtrl* ctrl = new ControllerCtrl();
	ctrl->init("data/data.json");
	std::cout << "Finished initial startup" << std::endl;
	std::cout << "----------------" << std::endl << std::endl;

	//add some items
	/* std::shared_ptr<Chat> chat = std::make_shared<Chat>(4L, "The Village", "@thevillage"); */
	/* ctrl->chatCtrl->addItem(chat); */
	/* ctrl->chatCtrl->addItem(std::make_shared<Chat>(5L, "The Village 2", "@thevillage2")); */

	std::cout<<"PRINTALL"<<std::endl;
	ctrl->chatCtrl->printAll();

	std::cout<<"JSONIFY"<<std::endl;
	std::cout << ctrl->chatCtrl->toJson() << std::endl;

	std::cout<<"GETBYID"<<std::endl;
	auto chat_ = ctrl->chatCtrl->getById(4L);

	std::cout << ctrl->toJson();

	std::cout << "----------------" << std::endl;
	delete ctrl;
	std::cout << "----------------" << std::endl;
}



