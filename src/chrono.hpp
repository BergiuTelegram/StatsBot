#include <chrono>

namespace std
{
	namespace chrono {
		typedef duration<int, ratio_multiply<hours::period, ratio<24> >::type> days;
	}
}

