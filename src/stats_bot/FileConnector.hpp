#pragma once
#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../json.hpp"

using json = nlohmann::json;

namespace stats_bot
{

	struct FileConnector
	{
		static bool write_to_file(std::string filename, std::string text)
		{
			std::ofstream of(filename);
			of << text;
		}

		static std::string read_file(std::string filename)
		{
			std::ifstream t(filename);
			std::string str( (std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>() );
			return str;
		}
	};// END FileConnector

}
