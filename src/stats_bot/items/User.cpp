#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "../../chrono.hpp"
#include "DayUserStat.hpp"
#include "Membership.hpp"
#include "User.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace items
	{

		User::User(long id, std::string name, bool is_bot, std::map<std::chrono::days, std::shared_ptr<DayUserStat>> dayUserStats, std::vector<std::shared_ptr<Membership>> memberships):
				Item(id), name(name), is_bot(is_bot), dayUserStats({}), memberships({})
				{
				}

		void User::print(std::ostream& os)const
		{
			os << this->name << " [" << this->id << "]";
		}

		json User::toJson()const
		{
			json out;
			out["id"] = this->id;
			out["name"] = this->name;
			out["is_bot"] = this->is_bot;
			for(auto i: this->dayUserStats)
			{
				out["ids_day_user_stats"].push_back(i.second->id);
			}
			for(auto i: this->memberships)
			{
				out["ids_memberships"].push_back(i->id);
			}
			return out;
		}

	}
}
