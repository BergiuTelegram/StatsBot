#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "UserChatStat.hpp"
#include "Membership.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace items
	{

		Membership::Membership(long id, bool removed, std::shared_ptr<UserChatStat> user_chat_stat):
				Item(id), removed(removed), user_chat_stat(user_chat_stat)
			{
			}

		void Membership::print(std::ostream& os)const
		{
			os << " \t- Messages: " << this->user_chat_stat->total_msg_cnt;
		}

		json Membership::toJson()const
		{
			json out;
			out["id"] = this->id;
			out["removed"] = this->removed;
			out["id_user_chat_stat"] = this->user_chat_stat->id;
			return out;
		}

	}
}
