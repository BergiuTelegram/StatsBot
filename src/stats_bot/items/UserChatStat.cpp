#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "UserChatStat.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace items
	{

		UserChatStat::UserChatStat(long id, long total_msg_cnt):
				Item(id), total_msg_cnt(total_msg_cnt)
			{
			}

		void UserChatStat::print(std::ostream& os)const
		{
			os << "Chat Stat [" << this->id << "] - Total Messages: " << this->total_msg_cnt;
		}

		json UserChatStat::toJson()const
		{
			json out;
			out["id"] = this->id;
			out["total_msg_cnt"] = this->total_msg_cnt;
			return out;
		}

	}
}
