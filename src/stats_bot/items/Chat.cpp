#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "../../chrono.hpp"
#include "DayChatStat.hpp"
#include "UserChatStat.hpp"
#include "Membership.hpp"
#include "Chat.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace items
	{

		Chat::Chat(long id, std::string title, std::string username,
		           std::map<std::chrono::days, std::shared_ptr<DayChatStat>>
		           dayChatStats,
		           std::vector<std::shared_ptr<Membership>> memberships):
			Item(id), title(title), username(username),
			dayChatStats(dayChatStats), memberships(memberships)
		{
		}

		void Chat::print(std::ostream &os)const
		{
			os << this->title << " [" << this->id << "] - " <<
			   this->username << ":";
		}

		json Chat::toJson()const
		{
			json out;
			out["id"]       = this->id;
			out["title"]    = this->title;
			out["username"] = this->username;

			for (auto i : this->dayChatStats) {
				out["ids_day_chat_stats"].push_back(i.second->id);
			}

			for (auto i : this->memberships) {
				out["ids_memberships"].push_back(i->id);
			}

			return out;
		}

	}
}
