#pragma once
#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace items
	{

		struct Item
		{
			long id;

			Item(long id=0);

			virtual json toJson()const = 0;

			virtual void print(std::ostream& where)const = 0;

			bool operator==(const Item& rechts);

			bool operator!=(const Item& rechts);

			friend std::ostream& operator<<(std::ostream& out, Item const & i)
			{
				i.print(out);
				return out;
			}

		};//END Item

	}
}
