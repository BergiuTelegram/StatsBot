#pragma once
#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "UserChatStat.hpp"
#include "Item.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace items
	{

		struct Membership: Item
		{
			bool removed;
			std::shared_ptr<UserChatStat> user_chat_stat;

			Membership(long id=0, bool removed=false, std::shared_ptr<UserChatStat> user_chat_stat=nullptr);

			void print(std::ostream& os)const;

			json toJson()const;

		};//END Membership

	}
}
