#pragma once
#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "../../chrono.hpp"
#include "Item.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace items
	{

		struct DayUserStat: Item
		{
			std::chrono::days date;
			long msg_count;

			DayUserStat(long id=0, std::chrono::days date=std::chrono::days(0), long msg_count=0);

			void print(std::ostream& os)const;

			json toJson()const;

		};//END DayUserStat

	}
}
