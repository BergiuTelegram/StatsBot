#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "../../chrono.hpp"
#include "DayUserStat.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace items
	{

		DayUserStat::DayUserStat(long id, std::chrono::days date, long msg_count):
				Item(id), date(date), msg_count(msg_count)
			{
			}

		void DayUserStat::print(std::ostream& os)const
		{
			os << "\n\t- Messages: " << this->msg_count;
		}

		json DayUserStat::toJson()const
		{
			json out;
			out["id"] = this->id;
			out["date"] = this->date.count();
			out["msg_count"] = this->msg_count;
			return out;
		}

	}
}
