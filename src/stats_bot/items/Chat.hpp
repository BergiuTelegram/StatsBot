#pragma once
#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "../../chrono.hpp"
#include "DayChatStat.hpp"
#include "UserChatStat.hpp"
#include "Membership.hpp"
#include "Item.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace items
	{

		struct Chat: Item
		{
			std::string title;
			std::string username;
			std::map<std::chrono::days, std::shared_ptr<DayChatStat>> dayChatStats;
			std::vector<std::shared_ptr<Membership>> memberships;

			Chat(long id=0, std::string title="", std::string username="", std::map<std::chrono::days, std::shared_ptr<DayChatStat>> dayChatStats={}, std::vector<std::shared_ptr<Membership>> memberships={});

			void print(std::ostream& os)const;

			json toJson()const;

		};//END Chat

	}
}
