#pragma once
#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "../../chrono.hpp"
#include "DayUserStat.hpp"
#include "Membership.hpp"
#include "Item.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace items
	{

		struct User: Item
		{
			std::string name;
			bool is_bot;
			std::map<std::chrono::days, std::shared_ptr<DayUserStat>> dayUserStats;
			std::vector<std::shared_ptr<Membership>> memberships;

			User(long id=0, std::string name="", bool is_bot=false, std::map<std::chrono::days, std::shared_ptr<DayUserStat>> dayUserStats={}, std::vector<std::shared_ptr<Membership>> memberships={});

			void print(std::ostream& os)const;

			json toJson()const;

		};//END User

	}
}
