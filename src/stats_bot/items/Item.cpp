#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "Item.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace items
	{

		Item::Item(long id)
			: id(id)
			{
			}

		bool Item::operator==(const Item& rechts)
		{
			return this->id == rechts.id;
		}

		bool Item::operator!=(const Item& rechts)
		{
			return !(*this==rechts);
		}

	}
}
