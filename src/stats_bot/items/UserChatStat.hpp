#pragma once
#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "Item.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace items
	{

		struct UserChatStat: Item
		{
			long total_msg_cnt;

			UserChatStat(long id=0, long total_msg_cnt=0);

			void print(std::ostream& os)const;

			json toJson()const;

		};//END UserChatStat

	}
}
