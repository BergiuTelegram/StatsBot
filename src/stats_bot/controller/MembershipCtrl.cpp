#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "../items/Membership.hpp"
#include "MembershipCtrl.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace controller
	{

		using namespace items;

		std::shared_ptr<Membership> MembershipCtrl::createSharedDummy(json j)
		{
			if (j == nullptr) {
				return nullptr;
			}
			auto t = std::make_shared<Membership>();
			t->id = j["id"];
			t->removed = j["removed"];
			return t;
		}

		void MembershipCtrl::linkObjects(std::shared_ptr<Membership> t, json j)
		{
			/* std::shared_ptr<UserChatStat> ucs = cc.userChatStatCtrl->getById(j["id_user_chat_stat"]); */
			/* if(ucs!=nullptr) */
			/* { */
			/* 	t->user_chat_stat = ucs; */
			/* } else { */
			/* 	//object does not exist */
			/* } */
		}

	}
}
