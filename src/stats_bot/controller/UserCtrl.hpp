#pragma once
#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "../items/User.hpp"
#include "Controller.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace controller
	{

		using namespace items;

		struct UserCtrl: Controller<User>
		{
			virtual std::shared_ptr<User> createSharedDummy(json j);

			virtual void linkObjects(std::shared_ptr<User> t, json j);
		};

	}
}
