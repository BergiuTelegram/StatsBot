#pragma once
#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "../items/UserChatStat.hpp"
#include "Controller.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace controller
	{

		using namespace items;

		struct UserChatStatCtrl: Controller<UserChatStat>
		{
			virtual std::shared_ptr<UserChatStat> createSharedDummy(json j);

			virtual void linkObjects(std::shared_ptr<UserChatStat> t, json j);
		};

	}
}
