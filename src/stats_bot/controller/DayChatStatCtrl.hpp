#pragma once
#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "../items/DayChatStat.hpp"
#include "Controller.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace controller
	{

		using namespace items;

		struct DayChatStatCtrl: Controller<DayChatStat>
		{
			std::shared_ptr<DayChatStat> getByDayAndChat(std::chrono::days days, long chat_id);

			virtual std::shared_ptr<DayChatStat> createSharedDummy(json j);

			virtual void linkObjects(std::shared_ptr<DayChatStat> t, json j);
		};

	}
}
