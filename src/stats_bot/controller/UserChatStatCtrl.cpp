#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "../items/UserChatStat.hpp"
#include "UserChatStatCtrl.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace controller
	{

		using namespace items;

		std::shared_ptr<UserChatStat> UserChatStatCtrl::createSharedDummy(json j)
		{
			if (j == nullptr) {
				return nullptr;
			}
			auto t = std::make_shared<UserChatStat>();
			t->id = j["id"];
			t->total_msg_cnt = j["total_msg_cnt"];
			return t;
		}

		void UserChatStatCtrl::linkObjects(std::shared_ptr<UserChatStat> t, json j)
		{
		}

	}
}
