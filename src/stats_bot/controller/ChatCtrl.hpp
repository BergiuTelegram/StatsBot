#pragma once
#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "../items/Chat.hpp"
#include "Controller.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace controller
	{

		using namespace items;

		struct ChatCtrl: Controller<Chat>
		{
			virtual std::shared_ptr<Chat> createSharedDummy(json j);

			virtual void linkObjects(std::shared_ptr<Chat> t, json j);
		};

	}
}
