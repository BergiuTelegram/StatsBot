#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "../items/DayUserStat.hpp"
#include "DayUserStatCtrl.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace controller
	{

		using namespace items;

		std::shared_ptr<DayUserStat> DayUserStatCtrl::getByDayAndUser(std::chrono::days days, long user_id)
		{
			for (std::shared_ptr<DayUserStat> i : this->list) {
				if (i->id == user_id && i->date == days) {
					return i;
				}
			}

			return nullptr;
		}

		std::shared_ptr<DayUserStat> DayUserStatCtrl::createSharedDummy(json j)
		{
			if (j == nullptr) {
				return nullptr;
			}
			auto t = std::make_shared<DayUserStat>();
			t->id = j["id"];
			t->date = std::chrono::days(j["date"]);
			t->msg_count = j["msg_count"];
			return t;
		}

		void DayUserStatCtrl::linkObjects(std::shared_ptr<DayUserStat> t, json j)
		{
			//TODO
		}

	}
}
