#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "DayChatStatCtrl.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace controller
	{

		using namespace items;

		std::shared_ptr<DayChatStat> DayChatStatCtrl::getByDayAndChat(std::chrono::days days, long chat_id)
		{
			for (std::shared_ptr<DayChatStat> i : this->list) {
				if (i->id == chat_id && i->date == days) {
					return i;
				}
			}

			return nullptr;
		}

		std::shared_ptr<DayChatStat> DayChatStatCtrl::createSharedDummy(json j)
		{
			if (j == nullptr) {
				return nullptr;
			}
			auto t = std::make_shared<DayChatStat>();
			t->id = j["id"];
			t->date = std::chrono::days(j["date"]);
			t->msg_count = j["msg_count"];
			return t;
		}

		void DayChatStatCtrl::linkObjects(std::shared_ptr<DayChatStat> t, json j)
		{
			//TODO
		}

	}
}
