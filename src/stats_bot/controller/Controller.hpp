#pragma once
#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "../items/Item.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace controller
	{

		using namespace items;

		struct ControllerCtrl;

		template<typename T>
		struct Controller {
			typedef T value_type;

			std::vector<std::shared_ptr<T>> list;

			/* json toJson()const; */

			/* void fromJson(json j, ControllerCtrl& cc); */

			virtual std::shared_ptr<T> createSharedDummy(json j) = 0;

			virtual void linkObjects(std::shared_ptr<T> t, json j) = 0;

			/* bool addItem(std::shared_ptr<T> item); */

			/* std::shared_ptr<T> getById(long id); */

			/* void printAll(); */

			json toJson()const
			{
				json out;

				for (std::shared_ptr<T> i : this->list) {
					out.push_back(i->toJson());
				}

				return out;
			}

			void fromJson(json j)
			{
				if (j != nullptr) {
					for (auto obj_json : j) {
						std::shared_ptr<T> t = this->createSharedDummy(obj_json);
						if(t==nullptr){
							//obj_json was null
							continue;
						}
						this->linkObjects(t, obj_json);
						this->addItem(t);
					}
				}
			}

			bool addItem(std::shared_ptr<T> item)
			{
				if (this->getById(item->id) != nullptr) {
					std::cout << "not added" << std::endl;
					return false;
				}

				this->list.push_back(item);
				return true;
			}

			std::shared_ptr<T> getById(long id)
			{
				for (std::shared_ptr<T> i : this->list) {
					if (i->id == id) {
						return i;
					}
				}

				return nullptr;
			}

			void printAll()
			{
				for (std::shared_ptr<T> i : this->list) {
					std::cout << *i << std::endl;
				}
			}

			void test(ControllerCtrl &cc)
			{
				cc.printAll();
			}
		};

	}
}
