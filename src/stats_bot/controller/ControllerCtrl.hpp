#pragma once
#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "Controller.hpp"
#include "DayChatStatCtrl.hpp"
#include "DayUserStatCtrl.hpp"
#include "UserChatStatCtrl.hpp"
#include "MembershipCtrl.hpp"
#include "UserCtrl.hpp"
#include "ChatCtrl.hpp"


using json = nlohmann::json;

namespace stats_bot
{
	namespace controller
	{

		using namespace items;

		struct ControllerCtrl
		{
			std::string backup_filename;
			std::shared_ptr<DayUserStatCtrl>  dayUserStatCtrl;
			std::shared_ptr<DayChatStatCtrl>  dayChatStatCtrl;
			std::shared_ptr<UserChatStatCtrl> userChatStatCtrl;
			std::shared_ptr<MembershipCtrl>   membershipCtrl;
			std::shared_ptr<UserCtrl>         userCtrl;
			std::shared_ptr<ChatCtrl>         chatCtrl;

			~ControllerCtrl();

			void init(std::string backup_filename);

			json toJson();

			void fromJson(json j);

			bool loadFromFile();

			bool saveToFile();
		};

	}
}
