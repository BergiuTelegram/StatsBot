#pragma once
#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "../items/Membership.hpp"
#include "Controller.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace controller
	{

		using namespace items;

		struct MembershipCtrl: Controller<Membership>
		{
			virtual std::shared_ptr<Membership> createSharedDummy(json j);

			virtual void linkObjects(std::shared_ptr<Membership> t, json j);
		};

	}
}
