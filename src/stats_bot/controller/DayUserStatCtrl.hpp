#pragma once
#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "../items/DayUserStat.hpp"
#include "Controller.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace controller
	{

		using namespace items;

		struct DayUserStatCtrl: Controller<DayUserStat>
		{
			std::shared_ptr<DayUserStat> getByDayAndUser(std::chrono::days days, long user_id);

			virtual std::shared_ptr<DayUserStat> createSharedDummy(json j);

			virtual void linkObjects(std::shared_ptr<DayUserStat> t, json j);
		};

	}
}
