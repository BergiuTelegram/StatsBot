#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "../items/Chat.hpp"
#include "ChatCtrl.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace controller
	{

		using namespace items;

		std::shared_ptr<Chat> ChatCtrl::createSharedDummy(json j)
		{
			if (j == nullptr) {
				return nullptr;
			}
			auto t = std::make_shared<Chat>();
			t->id = j["id"];
			t->title = j["title"];
			t->username = j["username"];
			return t;
		}

		void ChatCtrl::linkObjects(std::shared_ptr<Chat> t, json j)
		{
			//TODO
			/* for(long id: j["id_user_chat_stat"]) */
			{
				/* std::shared_ptr<UserChatStat> ucs = cc.userChatStatCtrl->getById(id); */
				/* if(ucs!=nullptr) */
				/* { */
				/* 	/1* t->user_chat_stats.push_back *1/ */
				/* } else { */
				/* 	//object does not exist */
				/* } */
			}
		}

	}
}
