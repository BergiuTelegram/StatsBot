#include <string>
#include <iostream>
#include <chrono>
#include <ostream>
#include <vector>
#include <memory>
#include <map>
#include <chrono>
#include <sstream>
#include <streambuf>
#include <fstream>
#include "../../json.hpp"
#include "../FileConnector.hpp"
#include "ControllerCtrl.hpp"

using json = nlohmann::json;

namespace stats_bot
{
	namespace controller
	{

		using namespace items;

		ControllerCtrl::~ControllerCtrl()
		{
			this->saveToFile();
		}

		void ControllerCtrl::init(std::string backup_filename)
		{
			this->backup_filename = backup_filename;
			std::cout << "Initializing all controllers" << std::endl;
			this->dayUserStatCtrl  = std::make_shared<DayUserStatCtrl>();
			this->dayChatStatCtrl  = std::make_shared<DayChatStatCtrl>();
			this->userChatStatCtrl = std::make_shared<UserChatStatCtrl>();
			this->membershipCtrl   = std::make_shared<MembershipCtrl>();
			this->userCtrl         = std::make_shared<UserCtrl>();
			this->chatCtrl         = std::make_shared<ChatCtrl>();
			this->loadFromFile();
		}

		json ControllerCtrl::toJson()
		{
			json j;
			j["DayUserStat"]  = this->dayUserStatCtrl->toJson();
			j["DayChatStat"]  = this->dayChatStatCtrl->toJson();
			j["UserChatStat"] = this->userChatStatCtrl->toJson();
			j["Membership"]   = this->membershipCtrl->toJson();
			j["User"]         = this->userCtrl->toJson();
			j["Chat"]         = this->chatCtrl->toJson();
			return j;
		}

		bool ControllerCtrl::loadFromFile()
		{
			if (this->backup_filename == "") {
				return false;
			}

			std::cout << "Loading from file: " << this->backup_filename << std::endl;
			std::string json_raw = FileConnector::read_file(this->backup_filename);

			if (json_raw != "") {
				json j = json::parse(json_raw);
				std::cout << "Load From File CC" << std::endl;
				std::cout << j << std::endl;
				this->fromJson(j);
			}
		}

		void ControllerCtrl::fromJson(json j)
		{
			this->dayUserStatCtrl->fromJson(j["DayUserStat"]);
			this->dayChatStatCtrl->fromJson(j["DayChatStat"]);
			this->userChatStatCtrl->fromJson(j["UserChatStat"]);
			this->membershipCtrl->fromJson(j["Membership"]);
			this->userCtrl->fromJson(j["User"]);
			this->chatCtrl->fromJson(j["Chat"]);
		}

		bool ControllerCtrl::saveToFile()
		{
			if (this->backup_filename == "") {
				return false;
			}

			std::cout << "Saving to file: " << this->backup_filename << std::endl;
			std::stringstream x;
			x << this->toJson();
			return FileConnector::write_to_file(this->backup_filename, x.str());
		}

	}
}
